
let core = {
    rand: 0,
    initRand: function(){
        core.rand = Math.random(1000)
    },
    iName: function(uid){
        //console.log("iName", uid)
        if(uid==null) return ''
        let i = uid.indexOf(':')
        return uid.substr(0, i)
    },
    iKey: function(uid){
        console.log("iKey", uid)
        if(uid == null) return uid
        let i = uid.indexOf(':')
        return uid.substr(i+1, uid.length)
    },
    extractTags : function(text){
        let tags = text.match(/(^|\s)(#[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\d-]+)/ig);
        console.log("extractTags", tags)
        if(tags != null)
        tags.forEach((tag, key) => {
            tags[key] = tag.replace(" ", "");
        })
        console.log("extractTags", tags)
        
        return tags
    },
    /*getNodePath: async function(uid){
        let nodeName = core.iName(uid)
        const rootNode = await Instance.findOne({'name': nodeName}) 
        return rootNode.url + ":" + rootNode.port
    },*/
    avatarUrl : function(pageUid){
        //console.log("avatarUrl uid", pageUid)
        if(pageUid == null) return ''
        if(localStorage.getItem("network") == null) return
        if(core.rand==0) core.initRand()
        
        let iName = core.iName(pageUid)
        let network = JSON.parse(localStorage.getItem("network"))
        let url = ''
        network.forEach(node => {
          if(node.name == iName) url = node.url + ":" + node.port + "/uploads/avatar/" + pageUid + ".png"
        });
        //console.log("avatar url found", url)
        return url + "?" + core.rand
    },
    avatarUrlDefault : function(pageUid){
        if(pageUid == null) return ''
        if(core.rand==0) core.initRand()
        //console.log("avatarUrl uid", pageUid)
        let iName = core.iName(pageUid)
        let network = JSON.parse(localStorage.getItem("network"))
        let url = ''
        network.forEach(node => {
          if(node.name == iName) url = node.url + ":" + node.port + "/default-avatar.png"
        });
        //console.log("avatar url found", url)
        return url + "?" + core.rand
    },
    postImageUrl : function(postUid){
        if(postUid == null) return ''
        if(core.rand==0) core.initRand()
        //console.log("avatarUrl uid", postUid)
        let iName = core.iName(postUid)
        let network = JSON.parse(localStorage.getItem("network"))
        let url = ''
        network.forEach(node => {
          if(node.name == iName) url = node.url + ":" + node.port + "/uploads/post/"
        });
        return url
    },
    postPdfUrl : function(pageUid, pdfName){
        if(pageUid == null) return ''
        if(core.rand==0) core.initRand()
        //console.log("avatarUrl uid", postUid)
        let iName = core.iName(pageUid)
        let network = JSON.parse(localStorage.getItem("network"))
        let url = ''
        network.forEach(node => {
          if(node.name == iName) url = node.url + ":" + node.port + "/uploads/pdf/" + pageUid + "/" + pdfName
        });
        return url
    },
    iconPageType: function(type){
        if(type == 'user') return 'account'
        if(type == 'group') return 'account-group-outline'
        if(type == 'assembly') return 'hexagon-multiple-outline'
        if(type == 'event') return 'calendar-clock'
        
    },

    isFollowing: function(context, pageUid){
        let found = false
        if(context.userPage.follows==null) return false
            context.userPage.follows.forEach((page) => {
            if(page.uid == pageUid) found = true
        })
        return found
    },
    isMyContact: function(context, pageUid){
        if(context.user.pages.length < 1) return false
        let found = false
        context.userPage.contacts.forEach((page) => {
            if(page.uid == pageUid) found = true
        }); return found
    },
    isBlacklisted: function(context, pageUid){
        let found = false
        if(context.userPage.blacklist==null) return false
        context.userPage.blacklist.forEach((page) => {
            if(page.uid == pageUid) found = true
        })
        return found
    },
    completeText: function(txt, hashtags, nl2brX=true){
        if(txt == null) return ""

        txt = txt.replace(/<[^>]*>/g, '');

        let allLinks = txt.match(/\bhttps?:\/\/\S+/gi)
        //console.log("allLinks", allLinks)
        if(allLinks != null)
        allLinks.forEach((link) => {
           //console.log("foeach allLinks", link, i)
           let l = link
           if(link.indexOf('fbclid=') > -1) link = link.slice(0, link.indexOf('fbclid=')-1)
           txt = txt.replace(l, link) 
           txt = txt.replace(link, '<a href="'+link+'" target="_blank">'+link+'</a>') 
        })
        
        if(hashtags != null && hashtags.length > 0)
            hashtags.forEach((tag)=>{
                txt = txt.replace(tag, '') //' <span data-tag="'+tag+'" class="span-hashtag">'+tag+'</span>')
            })

        if(nl2brX) txt = core.nl2br(txt)
        return txt
    },
    nl2br: function(str, is_xhtml){
      var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>';
      return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    },
    

}

export default core