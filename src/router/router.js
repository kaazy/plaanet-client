import Vue from 'vue'
import VueRouter from 'vue-router'
//import Home from '../views/home.vue'
//import Login from '../viewslogin.vue'
//import Register from '../views/register.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/home.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/register.vue')
  },
  {
    path: '/charter',
    name: 'charter',
    component: () => import('../views/charter.vue')
  },
  {
    path: '/manifest',
    name: 'manifest',
    component: () => import('../views/manifest.vue')
  },
  {
    path: '/plan',
    name: 'plan',
    component: () => import('../views/plan.vue')
  },
  {
    path: '/geocom',
    name: 'geocom',
    component: () => import('../views/info-geocom.vue')
  },
  {
    path: '/info-nodes',
    name: 'info-nodes',
    component: () => import('../views/info-nodes.vue')
  },
  {
    path: '/info-system',
    name: 'info-system',
    component: () => import('../views/info-system.vue')
  },
  {
    path: '/select-node',
    name: 'select-node',
    component: () => import('../views/select-node.vue')
  },
  {
    path: '/clear-local-storage',
    name: 'clear-local-storage',
    component: () => import('../views/clear-local-storage.vue')
  },
  {
    path: '/first-step',
    name: 'first-step',
    component: () => import('../views/first-step.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/map',
    name: 'map',
    component: () => import('../views/main.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/stream',
    name: 'stream',
    component: () => import('../views/main.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/users',
    name: 'users',
    component: () => import('../views/map.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/private/sendto',
    name: 'private-sendto',
    component: () => import('../views/formPrivate.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/private/sendto/:uid',
    name: 'private-sendto-uid',
    component: () => import('../views/formPrivate.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/private',
    name: 'private',
    component: () => import('../views/private.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/blacklist',
    name: 'blacklist',
    component: () => import('../views/blacklist.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/follows',
    name: 'follows',
    component: () => import('../views/follows.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/contacts',
    name: 'contacts',
    component: () => import('../views/contacts.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/list/:type',
    name: 'list-by-type',
    component: () => import('../views/pageList.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/post/:postUid',
    name: 'single-post',
    component: () => import('../views/singlePost.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/page/:pageUid/:mode',
    name: 'single-page-mode',
    component: () => import('../views/page.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/page/:pageUid/',
    name: 'single-page',
    component: () => import('../views/page.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/settings/:pageUid/',
    name: 'page-settings',
    component: () => import('../views/pageSettings.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/createPage',
    name: 'createPage',
    component: () => import('../views/createPage.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/myPages',
    name: 'myPages',
    component: () => import('../views/myPages.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/confidentiality',
    name: 'confidentiality',
    component: () => import('../views/informations/confidentiality.vue')
  },
  {
    path: '/legals', 
    name: 'legals',
    component: () => import('../views/informations/legals.vue')
  },
  {
    path: '/cgu',
    name: 'cgu',
    component: () => import('../views/informations/cgu.vue')
  },
  {
    path: '/admin',
    name: 'admin',
    component: () => import('../views/admin/dashboard.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/status',
    name: 'status',
    component: () => import('../views/admin/dashboard.vue')
  },
  {
    path: '/admin/network',
    name: 'admin-network',
    component: () => import('../views/admin/node-network.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/admin/socket',
    name: 'admin-socket',
    component: () => import('../views/admin/socket.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/init',
    name: 'init',
    component: () => import('../views/init.vue')
  },
  {
    path: '/boot-node',
    name: 'boot-node',
    component: () => import('../views/boot-node.vue')
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})

router.beforeEach((to, from, next) => {
  // TODO: Replace this with data from VueX store getter later.
  const isLoggedIn =
    localStorage && localStorage.getItem("access_token") != null;

  if (to.matched.some(route => route.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!isLoggedIn) {
      next({
        path: "/"
        // query: { redirect: to.fullPath } // Uncomment this to add redirect info to url.
      });
    } else {
      next();
    }
  } else {
    next(); // make sure to always call next()!
  }
});


import axios from 'axios'
axios.interceptors.request.use(
  (config) => {
    let token = localStorage.getItem('access_token');
    if (token) {
      config.headers['x-auth-token'] = token
    }

    return config;
  }, 

  (error) => {
    return Promise.reject(error);
  }
);

export default router

